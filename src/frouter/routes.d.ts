
type Component = import('vue-router').RouteComponent
type RouterOptions = import('vue-router').RouterOptions
type FileRoutePath = '/a'|'/b'
type FileRoutePathSub = FileRoutePath | ''

export type FileRoute<Meta = any> = {
    path: FileRoutePath
    name: string,
    meta?: Meta,
    component: Component | (() => Promise<Component>),
    children?: FileRoute[],
    redict?: RouterOptions['routes'][0]['redirect']
}


declare const routes: FileRoute[]
export default routes
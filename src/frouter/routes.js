/* eslint-disable quotes */
import __B from '../../src/views/b.vue'
import {a} from '../../src/views/var.js'

const routes= [
	{
		"path": "/a",
		"name": "/a",
		"component": () => import('../../src/views/a.vue')
	},
	{
		"path": "/b",
		"name": "/b",
		"meta": {
    withLayout: false,
    lazy: false,
        a,
    b: ''
    ,},
		"component": __B,
		"beforeEnter":  (to, from) => {
        console.log(to, from)
    }
	}
]
export default routes
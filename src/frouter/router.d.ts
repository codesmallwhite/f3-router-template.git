import { RouterOptions } from 'vue-router'
import { FileRoute } from './routes'

type Meta<T extends Record<any, any> = { _?: '_' }> = {
    title?: string,
    // 是否懒加载 默认true
    lazy?: boolean,
    // 是否携带layout(如果有的话) 默认true
    withLayout?: boolean
} & T
export const defineMeta: <T extends Record<any, any>>(meta: Meta<T>) => void



type FileRouterOptions = Partial<RouterOptions> & {
    routesHook?: (routes: (FileRoute | RouterOptions['routes'][0])[]) => RouterOptions['routes'] | void
}

declare const filerouter: (options?: FileRouterOptions) => import('vue-router').Router
export default filerouter
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import filerouter from './frouter/router'

createApp(App)
    .use(filerouter())
    .mount('#app')

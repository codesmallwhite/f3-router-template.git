import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import f3Router from 'f3-router'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), f3Router()],
})
